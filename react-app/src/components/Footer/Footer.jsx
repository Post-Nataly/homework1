import React from 'react';

function Footer() {
    return (
        <footer className="footer">
            <div className="footer__block">
                <div className="footer__left">
                    <b>© ООО «<span className="hat__logo-colortext">Мой</span>Маркет», 2018-2022.</b>
                    <div><span>Для уточнения информации звоните по номеру <a className="breadcrumbs__colorlink" href="tel:7900000000">+7 900 000
                        0000</a>,</span>
                        <span>а предложения по сотрудничеству отправляйте на почту <a className="breadcrumbs__colorlink"
                            href="mailto:partner@mymarket.com">partner@mymarket.com</a></span>
                    </div>
                </div>
                <div className="footer__right">
                    <a className="breadcrumbs__colorlink" href="#header">Наверх</a>
                </div>
            </div>
        </footer>
    );
}

export default Footer;