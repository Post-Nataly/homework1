import React, { useState } from 'react';
import './Configs.css';

function Configs() {
    const configs = [
        {
            id: 1,
            value: '126 ГБ',
        },
        {
            id: 2,
            value: '256 ГБ',
        },
        {
            id: 3,
            value: '512 ГБ',
        },
    ]

    const [selectedIdx, setSelectedIdx] = useState(0);
    const hadleClick = (index) => {
        setSelectedIdx(index)
    }

    return (
        <div className="block">
            <div className="block__header">Конфигурация памяти: 128 ГБ</div>
            <div className="block__selection">
                {configs.map((config, key) => {
                    return (
                <button className={
                    key === selectedIdx ? 'button-row__selected' : 'button-row'
                }
                    onClick={() => hadleClick(key)}
                    key={config.id}
                >
                    {config.value}
                </button>
                )}
                )}
            </div>
        </div>
    );
}

export default Configs