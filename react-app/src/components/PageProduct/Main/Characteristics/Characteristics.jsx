import React from 'react'
import Colors from './Colors/Colors'
import Configs from './Configs/Configs'
import Reviews from './Reviews/Reviews'
import PropertysProduct from './PropertysProduct/PropertysProduct'
import Description from './Description/Description'
import FormReview from './FormReview/FormReview'

function Characteristics() {
  return (
    <div>
      <div className="content content_margin">
        <div className="content__characteristics">
          <Colors />
          <Configs />
          <PropertysProduct />
          <Description />
          <div className="block-table">
            <div className="block-table__header">Сравнение моделей</div>
            <table className="block-table__table">
              <tr className="table-compare">
                <th className="table-compare__head">Модель</th>
                <th className="table-compare__head">Вес</th>
                <th className="table-compare__head">Высота</th>
                <th className="table-compare__head">Ширина</th>
                <th className="table-compare__head">Толщина</th>
                <th className="table-compare__head">Чип</th>
                <th className="table-compare__head">Объём памяти</th>
                <th className="table-compare__head">Аккумулятор</th>
              </tr>

              <tr className="table-compare">
                <td className="table-compare__cell">Iphone 11</td>
                <td className="table-compare__cell">194 грамма</td>
                <td className="table-compare__cell">150.9 мм</td>
                <td className="table-compare__cell">75.7 мм</td>
                <td className="table-compare__cell">8.3 мм</td>
                <td className="table-compare__cell">A13 Bionik chip</td>
                <td className="table-compare__cell">до 128 Гб</td>
                <td className="table-compare__cell">До 17 часов</td>
              </tr>
              <tr className="table-compare">
                <td className="table-compare__cell">Iphone 12</td>
                <td className="table-compare__cell">164 грамма</td>
                <td className="table-compare__cell">146.7 мм</td>
                <td className="table-compare__cell">71.5 мм</td>
                <td className="table-compare__cell">7.4 мм</td>
                <td className="table-compare__cell">A14 Bionik chip</td>
                <td className="table-compare__cell">до 256 Гб</td>
                <td className="table-compare__cell">До 19 часов</td>
              </tr>
              <tr className="table-compare">
                <td className="table-compare__cell">Iphone 13</td>
                <td className="table-compare__cell">174 грамма</td>
                <td className="table-compare__cell">146.7 мм</td>
                <td className="table-compare__cell">71.5 мм</td>
                <td className="table-compare__cell">7.65 мм</td>
                <td className="table-compare__cell">A15 Bionik chip</td>
                <td className="table-compare__cell">до 512 Гб</td>
                <td className="table-compare__cell">До 19 часов</td>
              </tr>
            </table>
          </div>
          <Reviews />
          <FormReview />
        </div>
      </div>
    </div>
  )
}

export default Characteristics