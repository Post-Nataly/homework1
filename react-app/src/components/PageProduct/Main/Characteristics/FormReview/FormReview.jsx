// import React from 'react'

function FormReview() {

    //     const [errorName, setErrorName] = useState();
    //     const [errorRating, setErrorRating] = useState();
    // const [errorForm, setErrorForm] = useState();

    // const name = document.querySelector('.input-review__name');
    // const rating = document.querySelector('.input-review__rating');

    // const name = useState(() => {
    //     const saved = localStorage.getItem("name");
    //     return saved || '';
    // });

    // const rating = useState(localStorage.getItem("rating"));

    // function handleSubmit() {
    //     if (name === "") {
    //         setErrorName('Вы забыли указать имя и фамилию!');
    //         setErrorRating(null);
    //         return;
    //     }

    //     if (rating < 1 || rating > 5) {
    //         setErrorName(null);
    //         setErrorRating('Оценка должна быть от 1 до 5!');
    //         return;
    //     }
    // }

    // function Validation() {
    //     if (!handleSubmit) {
    //         setErrorForm('Неправильно заполнена форма!');
    //         return;
    //     }
    // }

    // return (
    //     <div className="block block_padding">
    //         <div className="block__header"><b>Добавить свой отзыв</b></div>
    //         <form action="" className="block__form" onSubmit={handleSubmit}>
    //             <fieldset className="fieldset-review">
    //                 <div className="fieldset-review__field">
    //                     <input className="input-review__name" type="text" name="name"
    //                         placeholder="Имя и фамилия" onInput={handleSubmit}>
    //                         {errorName && <div>{errorName}</div>}</input>
    //                     <div className="input-review__error-name-no"></div>
    //                     <input className="input-review__rating" type="number" name="rating"
    //                         placeholder="Оценка" min="1" max="5" step="1" onInput={handleSubmit}>
    //                         {errorRating && <div>{errorRating}</div>}</input>
    //                     <div className="input-review__error-rating-no"></div>
    //                 </div>
    //                 <textarea className="textarea-review" name="review"
    //                     placeholder="Текст отзыва"></textarea>
    //                 <div className="textarea-review__error-textarea-review-no"></div>
    //                 <button className="reviews__button" type="submit">
    //                     <b>Добавить отзыв</b>
    //                     </button>
    //             </fieldset>
    //         </form>
    //     </div>);


    const buttonReview = document.querySelector(".block__form");
    const inputName = document.querySelector('.input-review__name');
    const inputRating = document.querySelector('.input-review__rating');
    const inputTextarea = document.querySelector(".textarea-review");
    const blockErrorName = document.querySelector(".input-review__error-name-no");
    const blockErrorRating = document.querySelector(".input-review__error-rating-no");
    const blockErrorTextarea = document.querySelector(".textarea-review__error-textarea-review-no");
    const buttonAddToBasket = document.querySelector(".important__basket-button");
    const counterProduct = document.querySelector(".hat__counter-product-no");

    document.body.onload = displayCheck;

    buttonReview.addEventListener("submit", (event) => {
        event.preventDefault();
        validationName();
    })

    addToBasket();

    function validationName() {
        let errorMSG = "";
        blockErrorName.classList.add("input-review__error-name");

        if (inputName.value === "") {
            errorMSG = 'Вы забыли указать имя и фамилию!';
            blockErrorName.innerText = errorMSG;
            inputName.addEventListener('click', () => {
                blockErrorName.style.display = "none";
            })

            return;

        } else if (inputName.value.length < 2) {
            errorMSG = 'Имя не может быть короче 2-х символов!';
            blockErrorName.innerText = errorMSG;
            inputName.addEventListener('click', () => {
                blockErrorName.style.display = "none";
            })

            return;

        } else blockErrorName.style.display = "none";

        localStorage.setItem("name", inputName.value);
        validationRating();
    }

    function validationRating() {
        let errorMSG = "";
        blockErrorRating.classList.add("input-review__error-rating");

        if (inputRating.value === "" || inputRating.value < 1 || inputRating.value > 5) {
            errorMSG = 'Оценка должна быть от 1 до 5!';
            blockErrorRating.innerText = errorMSG;

            return;

        } else blockErrorRating.style.display = "none";

        localStorage.setItem("rating", inputRating.value);
        validationTextarea();
    }

    function validationTextarea() {
        let errorMSG = "";
        blockErrorTextarea.classList.add("textarea-review__error-textarea-review");

        if (inputTextarea.value === "") {
            errorMSG = 'Введите текст отзыва!';
            blockErrorTextarea.innerText = errorMSG;

            return;

        } else blockErrorTextarea.style.display = "none";

        localStorage.setItem("review", inputTextarea.value);
    }

    function displayCheck() {
        if (localStorage.getItem('name')) {
            inputName.placeholder = localStorage.getItem('name');
        } if (localStorage.getItem('rating')) {
            inputRating.placeholder = localStorage.getItem('rating');
        } if (localStorage.getItem('review')) {
            inputTextarea.placeholder = localStorage.getItem('review');
        } if (localStorage.getItem('cart')) {
            counterProduct.innerText = localStorage.getItem('cart');
            buttonAddToBasket.classList.add("important__full-basket-button");
            buttonAddToBasket.innerText = "Товар уже в корзине";
            if (counterProduct.value === 1) {
                counterProduct.classList.add("hat__counter-product");
            }
        }
    }

    function addToBasket() {
        if (localStorage.getItem('cart')) {
            removeFromCart();
        } else addToCart();
    }

    function addToCart() {
        buttonAddToBasket.addEventListener("click", () => {
            buttonAddToBasket.classList.add("important__full-basket-button");
            buttonAddToBasket.innerText = "Товар уже в корзине";
            counterProduct.classList.remove("hat__counter-product-no");
            counterProduct.classList.add("hat__counter-product");
            counterProduct.innerText = "1";
            localStorage.setItem("cart", 1);
            removeFromCart();
        })
    }


    function removeFromCart() {
        buttonAddToBasket.addEventListener("click", () => {
            buttonAddToBasket.classList.remove("important__full-basket-button");
            buttonAddToBasket.classList.add("important__basket-button");
            buttonAddToBasket.innerText = "Добавить в корзину";
            counterProduct.classList.remove("hat__counter-product");
            counterProduct.classList.add("hat__counter-product-no");
            counterProduct.innerText = "";
            localStorage.setItem("cart", "");
            addToCart();
        })
    }
}

export default FormReview