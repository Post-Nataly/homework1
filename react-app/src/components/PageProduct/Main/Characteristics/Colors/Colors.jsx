import React, { useState } from 'react';
import './Colors.css';

function Colors() {
    const colors = [
        {
            id: 1,
            image: "./images/color-1.png",
            alt: "Красный",
        },
        {
            id: 2,
            image: "./images/color-2.png",
            alt: "Розовый",
        },
        {
            id: 3,
            image: "./images/color-3.png",
            alt: "Зеленый",
        },
        {
            id: 4,
            image: "./images/color-4.png",
            alt: "Синий",
        },
        {
            id: 5,
            image: "./images/color-5.png",
            alt: "Белый",
        },
        {
            id: 6,
            image: "./images/color-6.png",
            alt: "Черный",
        },
    ];
   
    let [selectedIdx, setSelectedIdx] = useState(3);
    const hadleClick = (index) => {
        setSelectedIdx(index)
    }


    return (
        <div className="block">
            <div className="block__header">Цвет товара: Синий</div>
            <div className="block__selection">
                {colors.map(function (color, key) {
                    return (
                        <button className={
                            key === selectedIdx ? 'button-img__selected' : 'button-img'
                                }
                                onClick={() => hadleClick(key)}
                                key={color.id}
                            >
                                <img className="button-img__picture" src={color.image} alt={color.alt} />
                        </button>
                    );
                })}
            </div>
        </div>
    );
}

export default Colors