import React from 'react'

function PropertyProduct(props) {
  const { property } = props;
  const circle = [
    {
      src: "./images/Маркер.png",
      alt: "маркер",
    },
  ];

  return (
    <div className="row-list">
    <img className="row__circle" src={circle.src} alt={circle.alt} />
    {`${property.name}: `}<b>{`${property.param}`}</b>
    </div>
    )
}

function PropertysProduct() {


  const propertys = [
    {
      id: 1,
      name: 'Экран',
      param: '6.1',
    },
    {
      id: 2,
      name: 'Операционная система',
      param: 'iOS 15',
    },
    {
      id: 3,
      name: 'Беспроводные интерфейсы',
      param: 'NFC, Bluetooth, Wi-Fi',
    },
    {
      id: 4,
      name: 'Встроенная память',
      param: '128 ГБ',
    },
    {
      id: 5,
      name: 'Процессор',
      param: 'Apple A15 Bionic',
    },
    {
      id: 6,
      name: 'Вес',
      param: '173 г',
    },
  ];

  return (
    <div className="block">
      <div className="block__header">Характеристики товара</div>
      <div className="block__list">
          {propertys.map((property) => (
            <PropertyProduct property={property} />
          ))}
      </div>
    </div>
  );
}

export default PropertysProduct