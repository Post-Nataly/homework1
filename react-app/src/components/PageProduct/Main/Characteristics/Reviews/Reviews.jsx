import React from 'react'

function Review(props) {
    const { review } = props;

    return (
        <div className="review">
            <img className="review__photo" src={review.image} alt="фото автора" />
            <div className="review__rightcol">
                <div className="review__name">
                    {`${review.name}`}
                </div>
                <div class="review__stars">
                    <img src={review.stars.star1} alt="star1" />
                    <img src={review.stars.star2} alt="star2" />
                    <img src={review.stars.star3} alt="star3" />
                    <img src={review.stars.star4} alt="star4" />
                    <img src={review.stars.star5} alt="star5" />
                </div>
                <div className='paragraf'>
                    <b>{`Опыт использования: `}</b>{review.information.experience}
                    <br /><b>{`Достоинства: `}</b>{review.information.dignity}
                    <br /><b>{`Недостатки: `}</b>{review.information.flaws}
                </div>
                {(review.id === review[review.length + 1]) ? null : <div className="separator" />}
            </div>
        </div>
    );
}

function Reviews() {
    const reviews = [
        {
            id: 1,
            name: "Марк Г.",
            image: "./images/автор1.png",
            information: {
                experience: 'менее месяца',
                dignity: `это мой первый айфон после после огромного количества телефонов на
            андроиде.
            всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма
            неплохая,
            ширик тоже на высоте.`,
                flaws: `к самому устройству мало имеет отношение, но перенос данных с
            андроида -
            адская вещь) а если нужно переносить фото с компа, то это только через
            itunes,
            который урезает
            качество фотографий исходное`,
            },
            stars:
            {
                star1: "./images/Звезда.png",
                star2: "./images/Звезда.png",
                star3: "./images/Звезда.png",
                star4: "./images/Звезда.png",
                star5: "./images/Звезда.png",
            },
        },
        {
            name: "Валерий Коваленко",
            image: "./images/автор2.png",
            information: {
                experience: 'менее месяца',
                dignity: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
                flaws: `Плохая ремонтопригодность`,
            },
            stars:
            {
                star1: "./images/Звезда.png",
                star2: "./images/Звезда.png",
                star3: "./images/Звезда.png",
                star4: "./images/Звезда.png",
                star5: "./images/Звезда2.png",
            },
        },
    ];

    return (
        <div className="reviews">

            <div className="reviews__title">
                <div className="reviews__text">
                    Отзывы
                    <div className="reviews__text reviews__text_grey">425</div>
                </div>
            </div>

            <div className="reviews__list">
                {reviews.map((review) => (
                    <div>
                        <Review review={review} />
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Reviews