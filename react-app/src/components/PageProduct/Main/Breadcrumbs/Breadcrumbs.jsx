import React from "react";
import "./Breadcrumbs.css";

function Breadcrumb(props) {
    const { breadcrumb } = props;

    return (
        <div className="breadcrumbs">
            <div className="breadcrumbs__colorlink">
                {`${breadcrumb.name}`}
            </div>
            {(breadcrumb.id !== 4) ? ">" : null}
        </div>
    );
}

function Breadcrumbs() {

    const breadcrumbs = [
        {
            id: 1,
            name: 'Электроника',
            href: 'file:http://localhost:3000/',
        },
        {
            id: 2,
            name: 'Смартфоны и гаджеты',
            href: 'file:///C:/Жена/иннополис/Домашка/homework1/index.html',
        },
        {
            id: 3,
            name: 'Мобильные телефоны',
            href: 'file:///C:/Жена/иннополис/Домашка/homework1/index.html',
        },
        {
            id: 4,
            name: 'Apple',
            href: 'file:///C:/Жена/иннополис/Домашка/homework1/index.html',
        }
    ];

    return (
        <div className="breadcrumbs">
            {breadcrumbs.map((breadcrumb, id) => (
                <Breadcrumb breadcrumb={breadcrumb} key={id} />
            ))}
        </div>
    )
}

export default Breadcrumbs