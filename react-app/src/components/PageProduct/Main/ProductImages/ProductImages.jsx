import React from 'react';
import "./ProductImages.css";

function ProductImages() {
    const products = [
        {
            id: 1,
            image: "./images/image-1.png",
            name: 'Экран+Камера',
        },
        {
            id: 2,
            image: './images/image-2.png',
            name: 'Экран',
        },
        {
            id: 3,
            image: './images/image-3.png',
            name: 'Вид с боку',
        },
        {
            id: 4,
            image: './images/image-4.png',
            name: 'Камера',
        },
        {
            id: 5,
            image: './images/image-5.png',
            name: 'Камера+Экран',
        },
    ];

    return (
        <div className="phone">
            <div className="phone__title">Смартфон Apple iPhone 13</div>
            <div className="phone__image">
                <div className="phone__img">
                    {products.map((product) => (
                        <img className="phone__img" src={product.image} alt={product.name} key={product.id}></img>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default ProductImages