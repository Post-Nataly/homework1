import React from "react";
import "./Main.css";
import Breadcrumbs from './Breadcrumbs/Breadcrumbs';
import ProductImages from "./ProductImages/ProductImages";
import Characteristics from "./Characteristics/Characteristics";
import Sidebar from "./Sidebar/Sidebar";

function Main() {
    return (
        <main className="page">
            <div className="page__section">
                <Breadcrumbs />
                <ProductImages />
            </div>
            <div className="content">
                <Characteristics />
                <Sidebar />
            </div>
        </main>
    );
}

export default Main