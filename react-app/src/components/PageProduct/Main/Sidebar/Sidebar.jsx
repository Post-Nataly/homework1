import React from 'react'

function Sidebar() {
    return (
        <div className="content__sidebar">
            <div className="important">
                <div className="important__prise">
                    <div className="important__different">
                        <div className="oldprise">
                            <div className="oldprise__price">75 990₽</div>
                            <div className="oldprise__discount">-8%</div>
                        </div>
                        <form className="like">
                            <label className="like__label">
                                <input className="like__heart" type="checkbox" name="like" />
                                <div className="like__heart-image" />
                            </label>
                        </form>
                    </div>
                    <div className="newprise"> 67 990₽</div>
                    <div className="important__delivery">
                        <div>Самовывоз в четверг, 1 сентября - <b>бесплатно</b></div>
                        <div>Курьером в четверг, 1 сентября - <b>бесплатно</b></div>
                    </div>
                    <button className="important__basket-button" name="cart">Добавить в корзину</button>
                </div>
            </div>

            <div className="blurb">Реклама
                <div className="blurb__list">
                    <iframe className="blurb__frame" src="/ads.html" title="onefrme" />
                    <iframe className="blurb__frame" src="/ads.html" title="toframe" />
                </div>
            </div>
        </div>
    )
}

export default Sidebar