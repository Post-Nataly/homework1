import React from 'react';
import './Center.css'

function Center() {
    return (
        <div className='pageOne'>
            <div className='message'>Здесь должно быть содержимое главной страницы.
                Но в рамках курса главная страница используется лишь
                в демонстрационных целях
            </div>
            <a href='http://localhost:3000/product' className='breadcrumbs__colorlink'>Перейти на страницу товара</a>
        </div>
    )
}

export default Center