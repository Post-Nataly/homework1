import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import PageIndex from "./components/PageIndex/PageIndex"
import PageProduct from './components/PageProduct/PageProduct';
import PageNotFound from "./components/PageNotFound/PageNotFound";
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<PageIndex />} />
        <Route path="/product" element={<PageProduct />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
      <Footer />
    </BrowserRouter>
  )
}

export default App;